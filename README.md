# Angular-cache-control #

The built-in cache support in AngularJS does not honor cache-control headers.  According to the docs at https://docs.angularjs.org/api/ng/service/$http, "A cache-control header on the response does not affect if or how responses are cached." Angular-cache-control attempts to fill in this gap by expiring content from the $http cache based on the cache-control headers in the response.

### What is this repository for? ###

* Angular-cache-control is an http interceptor for use in an AngularJS application to expire or remove content based on cache-control headers in the http response.
* Version 0.0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
    * http-cache-semantics <https://www.npmjs.com/package/http-cache-semantics>
    * parse-cache-control <https://www.npmjs.com/package/parse-cache-control>
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Steve Bering <sbering@soundphysicians.com>